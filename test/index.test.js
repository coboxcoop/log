const { describe } = require('tape-plus')
const collect = require('collect-stream')
const Multifeed = require('@coboxcoop/multifeed')
const Corestore = require('corestore')
const RAM = require('random-access-memory')
const Kappa = require('kappa-core')
const { Header } = require('hypertrie/lib/messages')
const memdb = require('level-mem')
const crypto = require('@coboxcoop/crypto')
const { encodings } = require('@coboxcoop/schemas')
const SpaceAbout = encodings.space.about

const Log = require('../')

const { replicate, tmp, cleanup } = require('./util')

describe('@coboxcoop/log', (context) => {
  context('constructor()', (assert, next) => {
    var log = Log({
      core: new Kappa(),
      feeds: new Multifeed(new Corestore(RAM)),
      db: memdb()
    })

    assert.ok(log.core instanceof Kappa, 'sets this.core')
    assert.ok(log.feeds, 'sets this.feeds')
    assert.ok(log.keyPair, 'sets this.keyPair')
    assert.ok(log.db, 'sets this.db')

    next()
  })

  context('ready()', async (assert, next) => {
    var log = Log({
      core: new Kappa(),
      feeds: new Multifeed(new Corestore(RAM)),
      db: memdb()
    })

    await log.ready()

    assert.ok(log._feed, 'creates a new hypercore')
    assert.ok(log.get, 'binds hypercore get() to log')
    assert.ok(log.append, 'binds hypercore append() to log')
    assert.ok(log.core.view.log, 'creates a view')
    assert.same(log.read, log.core.view.log.read, 'binds view.read() to read()')

    log.get(0, (err, msg) => {
      var header = Header.decode(msg)

      assert.same(header.type, 'log', 'appends a header')
      next()
    })
  })

  context('append()', async (assert, next) => {
    var log = Log({
      core: new Kappa(),
      feeds: new Multifeed(new Corestore(RAM)),
      db: memdb()
    })

    await log.ready()

    var msg = {
      type: 'space/about',
      version: '1.0.0',
      timestamp: Date.now(),
      author: crypto.randomBytes(32).toString('hex'),
      content: { name: 'magma' }
    }

    log.append(SpaceAbout.encode(msg), (err, seq) => {
      assert.error(err, 'no error')
      assert.same(1, seq, 'success')
      next()
    })
  })

  context('publish()', async (assert, next) => {
    var author = crypto.randomBytes(32).toString('hex')

    var log = Log({
      core: new Kappa(),
      feeds: new Multifeed(new Corestore(RAM)),
      db: memdb()
    })

    await log.ready()

    var msg = {
      type: 'space/about',
      version: '1.0.0',
      timestamp: Date.now(),
      author: crypto.randomBytes(32).toString('hex'),
      content: { name: 'magma' }
    }

    const data = await log.publish(msg, { valueEncoding: SpaceAbout })

    assert.ok(data.key, 'has key')
    assert.same(data.seq, 1, 'seq is 1')
    assert.ok(Buffer.isBuffer(data.value), 'value returns the payload')
    assert.same(msg, SpaceAbout.decode(data.value), 'value is the message')
    next()
  })

  context('read()', async (assert, next) => {
    var log = Log({
      core: new Kappa(),
      feeds: new Multifeed(new Corestore(RAM)),
      db: memdb()
    })

    await log.ready()

    var timestamp = Date.now()
    var message = {
      type: 'space/about',
      version: '1.0.0',
      timestamp,
      author: crypto.randomBytes(32).toString('hex'),
      content: { name: 'magma' }
    }

    log.append(SpaceAbout.encode(message), (err, seq) => {
      assert.error(err, 'no error')
      assert.same(1, seq, 'appends the payload')

      log.get(seq, (err, msg) => {
        assert.error(err, 'no error')
        assert.same(message, SpaceAbout.decode(msg), 'gets the payload')

        var check = [{ key: log._feed.key.toString('hex'), seq: 1, value: message }]
        log.ready(() => {
          collect(log.read({ query: [{ $filter: { value: { type: 'space/about' } } }] }), (err, msgs) => {
            assert.error(err, 'no error')
            assert.same(msgs.length, 1, 'gets one message')
            assert.same(msgs, check, 'message matches')
            next()
          })
        })
      })
    })
  })

  context('replication() + read()', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()
    var key = crypto.randomBytes(32)

    var log1 = Log({
      core: new Kappa(),
      feeds: new Multifeed(new Corestore(RAM), key),
      db: memdb()
    })

    var log2 = Log({
      core: new Kappa(),
      feeds: new Multifeed(new Corestore(RAM), key),
      db: memdb()
    })

    var msg1 = {
      type: 'space/about',
      version: '1.0.0',
      timestamp: Date.now(),
      author: crypto.randomBytes(32).toString('hex'),
      content: { name: 'blockades' }
    }

    var msg2 = {
      type: 'space/about',
      version: '1.0.0',
      timestamp: Date.now() + 1,
      author: crypto.randomBytes(32).toString('hex'),
      content: { name: 'magma' }
    }

    append(log1, msg1, () => {
      sync(() => {
        check(() => {
          append(log2, msg2, () => {
            sync(checkAgain)
          })
        })
      })
    })

    function append (log, msg, cb) {
      log.ready((err) => {
        assert.error(err, 'no error')
        log.append(SpaceAbout.encode(msg), (err, seq) => {
          assert.error(err, 'no error')
          cb()
        })
      })
    }

    function sync (cb) {
      log1.ready(() => log2.ready(() => {
        replicate(log1.feeds, log2.feeds, (err) => {
          assert.error(err, 'no error on replicate')
          log1.ready(() => log2.ready(cb))
        })
      }))
    }

    function check (cb) {
      var query = [{ $filter: { value: { type: 'space/about' } } }]
      collect(log2.read({ query }), (err, msgs) => {
        assert.error(err, 'no error')
        var check = [{ key: log1._feed.key.toString('hex'), seq: 1, value: msg1 }]
        assert.same(msgs.length, 1, 'gets one message')
        assert.same(msgs, check, 'message matches')
        cb()
      })
    }

    function checkAgain () {
      var query = [{ $filter: { value: { type: 'space/about' } } }]
      collect(log1.read({ query }), (err, msgs) => {
        assert.error(err, 'no error')
        assert.same(msgs.length, 2, 'gets two messages')
        assert.same(msgs, [
          { key: log1._feed.key.toString('hex'), seq: 1, value: msg1 },
          { key: log2._feed.key.toString('hex'), seq: 1, value: msg2 }
        ], 'messages matches')
        cleanup([storage1, storage2], next)
      })
    }
  })
})
