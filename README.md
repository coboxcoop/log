# log

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

  - [About](#about)
  - [Install](#install)
  - [Usage](#usage)
  - [API](#api)
  - [Contributing](#contributing)
  - [License](#license)

## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`log` provides a multifeed source for all cobox logs. Uses `kappa-core@experimental` and `kappa-view-query` to establish a queryable set of indices.

A cobox-log hypercore feed is initially appended with a `hypertrie` header, that simply gives the feed the name `log`. This allows other peers to identify what kind of content the feed holds and index the feed correctly, and vice-versa, allows you to index other peer's log feeds correctly.

A CoBox log can be identified with a header appended as the first entry to the feed.

## Install 
```
npm i -g @coboxcoop/log
```

## Usage

```js
const Kappa = require('kappa-core')
const multifeed = require('multifeed')
const { Header } = require('hypertrie/lib/messages')
const crypto = require('hypercore-crypto')
const mkdirp = require('mkdirp')
const tmpdir = require('tmpdir')
const level = require('level')

const tmp = tmpdir().name
mkdirp.sync(tmp)

// author is a global keypair used to sign all records
// it is more akin to an identity than a log's signing keypairs
// in cobox, this is derived from a parent key

// var author = crypto.keyPair().publicKey
var author = '5c448d8aecdc013d9145b6e936f9b255434e7f9e728213486f0b18f211512fb2'

const log = new Log({
  core: new Kappa(),
  feeds: multifeed(ram),
  keyPair: crypto.keyPair(),
  author,
  db: level(tmp)
})

log.get(0, (err, payload) => {
  Header.decode(payload)
  // { type: 'log', metadata: null }
})

var msg = {
  type: 'peer/about',
  version: '1.0.0',
  timestamp: 1580813687256,
  author,
  content: { name: 'magpie' }
}

log.publish(payload, { valueEncoding: PeerAbout }, (err, msg) => {
  // {
  //   key: '031fb3bd3c2571a7ca54a01a1021e374fd63d0b56b1a9ab142b9ae7a24287dc8@1',
  //   seq: 1,
  //   value: {
  //     type: 'peer/about',
  //     timestamp: 1580813687256,
  //     author: '5c448d8aecdc013d9145b6e936f9b255434e7f9e728213486f0b18f211512fb2',
  //     content: {
  //       name: 'magpie'
  //     }
  //   }
  // }
})
```

## API
See swagger documentation... (we won't have this for a while).

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

# License

[`AGPL-3.0-or-later`](./LICENSE)
